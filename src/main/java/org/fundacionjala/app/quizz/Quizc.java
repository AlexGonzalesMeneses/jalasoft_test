package org.fundacionjala.app.quizz;

public class Quizc {

    public void run() {
        Menu menu = new Menu();
        //boolean shouldExit = false;
        boolean shouldExit = true;
        do {
            shouldExit = menu.process();
        } while (!shouldExit);
    }
}
