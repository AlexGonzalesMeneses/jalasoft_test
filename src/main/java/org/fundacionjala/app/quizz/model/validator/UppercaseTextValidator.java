package org.fundacionjala.app.quizz.model.validator;

import java.util.List;

public class UppercaseTextValidator implements Validator<String, String> {
    private static final String ERROR_MESSAGE = "input must only contain uppercase letters ";

    @Override
    public void validate(String value, String conditionValue, List<String> errors) {
        if (!value.equals(value.toUpperCase())){
            errors.add(ERROR_MESSAGE + conditionValue);
        }
    }
}
